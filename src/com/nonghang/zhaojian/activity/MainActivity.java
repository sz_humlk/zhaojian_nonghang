package com.nonghang.zhaojian.activity;

import java.text.DecimalFormat;

import android.os.Bundle;
import android.app.Activity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	EditText JinEs = null, Days = null, Percents = null;

	Button OK = null, CLEAR = null;

	TextView Result = null;

	double jine = 0, day = 0, percent = 0;

	String result = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		initView();

		pressOk();
	}

	private void initView() {
		JinEs = (EditText) findViewById(R.id.jine_shuju);
		Days = (EditText) findViewById(R.id.days_shuju);
		Percents = (EditText) findViewById(R.id.baifenbi_shuju);
		OK = (Button) findViewById(R.id.ok);
		CLEAR = (Button) findViewById(R.id.clear);
		Result = (TextView) findViewById(R.id.result);

	}

	private void pressOk() {
		OK.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {

				cancelInput();

				String jinestring = JinEs.getText().toString().trim();
				if (TextUtils.isEmpty(jinestring)) {
					JinEs.requestFocus();
					JinEs.setError("请输入！");
					return;
				}
				String daystring = Days.getText().toString().trim();
				if (TextUtils.isEmpty(daystring)) {
					Days.requestFocus();
					Days.setError("请输入！");
					return;
				}
				String percentstring = Percents.getText().toString().trim();
				if (TextUtils.isEmpty(percentstring)) {
					Percents.requestFocus();
					Percents.setError("请输入数字！可以有小数点");
					return;
				}

				parseResult(jinestring, daystring, percentstring);

				viewResult();

			}
		});

		CLEAR.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					cancelInput();
				} catch (Exception e) {
					// e.printStackTrace();
				}
				JinEs.setText("");
				Days.setText("");
				Percents.setText("");
				Result.setText("");
				JinEs.requestFocus();
			}

		});
	}

	private void parseResult(String jinestring, String daystring,
			String percentstring) {
		// double jine = 0, day = 0, percent = 0;
		jine = Double.parseDouble(jinestring);
		day = Double.parseDouble(daystring);
		try {
			percent = Double.parseDouble(percentstring);
		} catch (Exception e) {
			// e.printStackTrace();
			Percents.requestFocus();
			Percents.setError("只能输入数字！可以有小数点");
			return;
		}
		result = getResult(jine, day, percent);
	}

	private void viewResult() {
		Result.setText("\n" + getString(R.string.results) + result + "（元）");
	}

	/**
	 * 计算公式是：金额*百分比除以365*天数 金额的单位是万，但最终的结果是元
	 */
	private String getResult(double number, double percent, double days) {
		double a1 = number * 100 * percent;
		double a2 = a1 * days;
		double a3 = a2 / 365;
		DecimalFormat dcmFmt = new DecimalFormat("0.00");
		String a4 = dcmFmt.format(a3);
		return a4;
	}

	private void cancelInput() {
		((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
				.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus()
						.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
}
